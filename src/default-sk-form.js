
import { SkFormImpl }  from '../../sk-form/src/impl/sk-form-impl.js';

export class DefaultSkForm extends SkFormImpl {

    get prefix() {
        return 'default';
    }

    get suffix() {
        return 'form';
    }

    beforeRendered() {
        super.beforeRendered();
        this.attachStyleByPath(this.comp.skTheme.basePath + '/default.css', this.comp.querySelector('span[slot=fields]'));
    }

    afterRendered() {
        super.afterRendered();
        this.mountStyles();
    }


}
